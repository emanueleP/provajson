package com;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaController {

    @RequestMapping(value = "/persona", method = RequestMethod.GET)
    public ResponseEntity<Persona> getPersona() {
        return new ResponseEntity<>(
                new Persona(
                        new Dati("Prova", "Prova"),
                        new AltriDati("Via Tale", "333")),
                HttpStatus.OK
        );
    }
}
