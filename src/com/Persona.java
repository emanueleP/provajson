package com;

public class Persona {
    private Dati dati;
    private AltriDati altriDati;

    public Persona(){}

    public Persona(Dati dati, AltriDati altriDati) {
        this.dati = dati;
        this.altriDati = altriDati;
    }

    public Dati getDati() {
        return dati;
    }

    public void setDati(Dati dati) {
        this.dati = dati;
    }

    public AltriDati getAltriDati() {
        return altriDati;
    }

    public void setAltriDati(AltriDati altriDati) {
        this.altriDati = altriDati;
    }
}
class Dati{
    private String nome;
    private String cognome;

    public Dati(){}

    public Dati(String nome, String cognome) {
        this.nome = nome;
        this.cognome = cognome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }
}
class AltriDati{
    private String indirizzo, telefono;

    public AltriDati(){}

    public AltriDati(String indirizzo, String telefono) {
        this.indirizzo = indirizzo;
        this.telefono = telefono;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}