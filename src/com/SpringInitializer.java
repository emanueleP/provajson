package com;

import com.PersonaConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class SpringInitializer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext container) throws ServletException {
        AnnotationConfigWebApplicationContext servletContext = new AnnotationConfigWebApplicationContext();  //Spring web applicaton context
        servletContext.register(PersonaConfig.class); //classe di configurazione
        servletContext.setServletContext(container);

        ServletRegistration.Dynamic servletRegistration = container.addServlet("springDispatcher", new DispatcherServlet(servletContext));

        servletRegistration.setLoadOnStartup(1);
        servletRegistration.addMapping("/");
    }
}
